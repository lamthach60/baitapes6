const houseColor = ["red", "blue", "yellow", "green", "pink", "gray", "orange"];
const renderButton = () => {
  let contentHTML = "";
  for (var index = 0; index < houseColor.length; index++) {
    let color = houseColor[index];
    let contentBTN = `<button onclick="changeColor('${color}')" class="btn text-white" style="background-color:${color};height:40px;width:40px"></button>`;
    contentHTML = contentHTML + contentBTN;
  }
  document.getElementById("colorContainer").innerHTML = contentHTML;
  console.log(" contentHTML: ", contentHTML);
};
renderButton();

const changeColor = (color) => {
  document.getElementById("house").style.backgroundColor = color;
  document.getElementById("house-base").style.backgroundColor = color;
  document.getElementById("house-sandwich").style.backgroundColor = color;
  document.getElementById("house-upper").style.backgroundColor = color;
  document.getElementById("house-roof").style.backgroundColor = color;
  document.getElementById("house-upper-detail").style.backgroundColor = color;
  document.getElementById("house-upper-shadow").style.backgroundColor = color;
  document.querySelector(".house").style.setProperty("--primary-color", color);
};
